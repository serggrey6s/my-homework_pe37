let tabs = document.querySelectorAll(".tabs-title");
let tabsText = document.querySelectorAll(".tabs-content-items");

tabs.forEach(function(element) {
    element.addEventListener("click", function(){
        let current = element;
        let tabId = current.getAttribute("data-title");
        let currentTab = document.querySelector(tabId);
        tabs.forEach(function(element){
            element.classList.remove("active");
        })
        tabsText.forEach(function(element){
            element.classList.remove("active");
        })
        current.classList.add("active")
        currentTab.classList.add("active")
    })
})
