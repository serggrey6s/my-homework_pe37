//----------Обьяснить своими словами разницу между обьявлением переменных через var, let и const.----------

// 1. Var - имеет видимость глобальную и блочную (старый способ объявления)
// 2. Let и const - имеют видимость только блочную (новый способ объявления)
// 3. Var и let - значения можно переприсваевать, а вот в const - значения должны быть константой
//  и не переприсваиваются.


//----------Почему объявлять переменную через var считается плохим тоном?----------

// Потому что на данный момент мы имеем новую переменную let, 
// о старой мы должны знать так как она может попадаться в старых проектах


// Задача 1:

// Узнать с помощью модельного окна браузера данные пользователя: имя и возраст.
// Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
// Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. 
// Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя. 
// Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
// Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.



let userName = prompt('Write your name, please!');
let userAge = +prompt('Write your age, please!', 18);

if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if (userAge >= 18 && userAge <= 22) {
   let enter = confirm ('Are you sure you want to continue?');
   if (enter === true) {
       alert ("Welcome, " +userName);
   } else { alert('You are not allowed to visit this website.');}
   
} else if (userAge > 22) {
    alert("Welcome, " +userName);
}