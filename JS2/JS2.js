//--------------Описать своими словами в несколько строчек, зачем в программировании нужны циклы.--------------


// Циклы предусмотрены для многократного выполнения одного и того же отрезка кода, 
// благодаря циклам нам не стоит каждый раз писать/дублировать код, 
// а лиш стоит этот код зациклировать и будет он работать в цикле пока получает значение true 
// и как только получит false цикл будет остановлен.



//Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в заданном диапазоне. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// Считать с помощью модального окна браузера число, которое введет пользователь.
// Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


let userNumber = +prompt("Enter your number, please");

for (let begin = 0; begin <= userNumber; begin++){
    if (begin % 5 === 0){
        console.log(begin);
    } else {
        console.log(`Sorry, no numbers`)
    }
}
