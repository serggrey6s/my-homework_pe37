//Опишите своими словами, как Вы понимаете, что такое обработчик событий.

//это фуекция которая отселживает состояние клавиш ее нажатие и отпуск.


//Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). 
// Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
// В папке img лежат примеры реализации поля ввода и создающегося span.


let createDiv = document.createElement("div");
let createInput = document.createElement("input");
let span = document.createElement("span");
let text = document.createElement("p");
let createButton = document.createElement("button");

span.style.display = "inline-block";
createInput.id = "input";
createInput.placeholder = "price";
text.textContent = "Please, enter corect price ";
document.body.prepend(createDiv);
document.body.prepend(createDiv);
createDiv.prepend(createInput);


createInput.onfocus = () => {
    createInput.style.border = "5px solid green"
    span.remove();
    createButton.remove();
    text.remove();
}
createInput.onblur = () => {
    createInput.style = "";
    if (createInput.value > 0)  {
        text.remove();
        span.textContent = `Текущая цена: ${createInput.value} $ `;
        createButton.textContent = "x";
        document.body.prepend(createButton);
        document.body.prepend(span);
        createButton.addEventListener("click", () => {
            span.remove();
            createButton.remove();
            createInput.value = "";
        })
    } else if(createInput.value === ""){
        text.remove();
    } else {
        createInput.style.border = "5px solid red"
        createDiv.after(text);
    }
}
